import "antd/dist/antd.css";
import "./style.scss";
import axios from "axios";
/**page */
import Recipe from "./page/Recipe";

/**axios config */
import { baseUrl } from "./env";
axios.defaults.baseURL = baseUrl;

function App() {
  return (
    <div className="c-main">
      <div className="c-main__wrap">
        <Recipe />
      </div>
    </div>
  );
}

export default App;
