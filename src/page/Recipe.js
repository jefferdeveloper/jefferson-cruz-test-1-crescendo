import React, { useState, useEffect } from "react";
import { recipeFind, recipeFindCancel } from "../api/recipe";
import { specialFind, specialFindCancel } from "../api/special";
import { Card, Modal, Tag, Popover } from "antd";
import { LinkOutlined } from "@ant-design/icons";
import { baseUrl } from "../env";

export default function Recipe() {
  const [recipes, _recipes] = useState(null);
  const [modalVisible, _modalVisible] = useState(false);
  const [viewData, _viewData] = useState(null);

  useEffect(() => {
    getData();
    return () => {
      recipeFindCancel();
      specialFindCancel();
    };
    // eslint-disable-next-line
  }, []);

  const getData = async () => {
    try {
      const recipeData = await recipeFind();
      const specialData = await specialFind();
      if (recipeData && specialData) {
        const data = recipeData.map((r) => {
          const ingredients = r.ingredients.map((i) => {
            return {
              ...i,
              special: specialData.find((s) => s.ingredientId === i.uuid),
            };
          });
          return {
            ...r,
            ingredients,
          };
        });
        _recipes(data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const modalOpen = () => {
    _modalVisible(true);
  };

  const modalClose = () => {
    _modalVisible(false);
    _viewData(null);
  };

  const viewOpen = (uuid) => {
    const selectedRecipe = recipes.find((d) => d.uuid === uuid);
    if (selectedRecipe) {
      _viewData(selectedRecipe);
      modalOpen();
    }
  };

  return (
    <div>
      <div className="c-recipe">
        <h1>Recipes</h1>
        <div className="c-recipe__items">
          {recipes &&
            recipes.map(({ uuid, title, description, images }) => (
              <div
                className="c-recipe__item"
                key={uuid}
                onClick={() => viewOpen(uuid)}
              >
                <Card hoverable>
                  <div
                    className="o-thumbnail"
                    style={{
                      backgroundImage: `url(${baseUrl}${images.medium})`,
                    }}
                  ></div>
                  <Card.Meta title={title} description={description} />
                  <div className="o-btn__wrap">
                    <div className="o-btn o-btn--arrow_right">View Recipe</div>
                  </div>
                </Card>
              </div>
            ))}
        </div>

        <Modal
          className="c-modal"
          title="View Recipe"
          visible={modalVisible}
          centered
          width="100%"
          footer={null}
          onCancel={modalClose}
        >
          {viewData && (
            <div className="c-view">
              <div className="c-view__header">
                <div className="c-view__title">{viewData.title}</div>
                <div className="c-view__date">
                  <span>Date updated:</span> {viewData.editDate}
                </div>
              </div>
              <div
                className="o-thumbnail"
                style={{
                  backgroundImage: `url(${baseUrl}${viewData.images.full})`,
                }}
              ></div>
              <div className="c-view__description">
                <p>{`"${viewData.description}"`}</p>
              </div>
              <div className="c-estimate">
                <div className="c-estimate__info">
                  <span>Servings:</span> {viewData.servings}
                </div>
                <div className="c-estimate__info">
                  <span>Preparation Time:</span> {viewData.prepTime} mins
                </div>
                <div className="c-estimate__info">
                  <span>Cooking Time:</span> {viewData.cookTime} mins
                </div>
              </div>
              <div className="c-ingredient">
                <h3>Ingredients:</h3>
                <ul>
                  {viewData.ingredients.map(
                    ({ uuid, amount, measurement, name, special }) => (
                      <li key={uuid}>
                        {`${amount} ${measurement} of ${name}`}{" "}
                        {special ? (
                          <Popover
                            content={() => (
                              <div className="c-pop">
                                <div className="c-pop__text">
                                  {special.text}
                                </div>
                                {special.type === "event" ||
                                special.type === "local" ? (
                                  <a
                                    className="c-pop__link"
                                    rel="noreferrer"
                                    href={`http://maps.google.com/maps?q=${special.geo}`}
                                    target="_blank"
                                  >
                                    Click to check location
                                  </a>
                                ) : null}

                                {special.type === "promocode" ? (
                                  <div className="c-pop__code">
                                    {special.code}
                                  </div>
                                ) : null}
                              </div>
                            )}
                            title={special.title}
                            trigger="hover"
                          >
                            <Tag
                              className="c-tag"
                              icon={<LinkOutlined />}
                              color={
                                special.type === "event"
                                  ? "blue"
                                  : special.type === "local"
                                  ? "green"
                                  : special.type === "promocode"
                                  ? "red"
                                  : "gold"
                              }
                            >
                              special ({special.type})
                            </Tag>
                          </Popover>
                        ) : null}
                      </li>
                    )
                  )}
                </ul>
              </div>
              <div className="c-direction">
                <h3>Directions:</h3>
                <ol>
                  {viewData.directions.map(
                    ({ instructions, optional }, index) => (
                      <li key={index}>
                        {instructions}{" "}
                        {optional ? <span>(optional)</span> : null}
                      </li>
                    )
                  )}
                </ol>
              </div>
            </div>
          )}
        </Modal>
      </div>
    </div>
  );
}
